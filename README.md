# homelab
This project contains all of scripts used to configure the infrastructure and services on my home network.
Ansible, Cloud-Init, and Terraform are used to automatically provision and configure everything.

# Table of Contents
- [Inventory List](#inventory-list)
    - [Asus Router RT-ARCH13](#asus-router-rt-arch13)
    - [Raspberry Pi 4 Model B](#raspberry-pi-4-model-b)
- [Getting Started](#getting-started)
    - [1. lxd-01 / lxd-02](#lxd-01-/-lxd-02)
        - [Flash Image to microSD Card](#flash-image-to-microsd-card)
        - [Provision with Cloud-init](#provision-with-cloud-init)
        - [Configure with Ansible Playbook](#configure-with-ansible-playbook)

## Inventory List
This section describes the physical hardware used in my home network.

### Asus Router RT-ARCH13
This device acts as the primary gateway for my home network.

Networking:
- Hostname = openwrt.dudelab.net
- IP = 192.168.50.1

Specifications:
- Hardware: 128MB FLASH, 128MB RAM
- Operating System: OpenWRT

### Raspberry Pi 4 Model B
Two of these devices serve together as an LXD cluster.

Networking:
- Hostname = lxd-\[01,02\].dudelab.net
- IP = 192.168.50.\[12,13\]

Specifications:
- Hardware: 32GB MICROSD, 4GB RAM
- Operating System: Ubuntu Server 20.04 LTS

## Getting Started
This section describes how to completely stand up my home infrastructure from scratch.

Create a SSH key pair for the ansible user at `~/.ssh/ansible/`.

Create the ansible vault password file at `ansible/.ansible_vault_pass`.

### lxd-01 / lxd-02
This section describes how to set up lxd-01 and lxd-02 from scratch.

#### Flash Image to microSD Card
1. Download the 64-bit Ubuntu 20.04.1 LTS image for Raspberry Pi 4 from https://ubuntu.com/download/raspberry-pi.

2. Flash the image to a microSD card using a tool like balenaEtcher.

#### Provision with Cloud-init
3. Copy `cloud-init-files/lxd-01-network-config.yml` to `SD:/system-boot/network-config`.

4. Copy `cloud-init-files/lxd-01-user-data.yml` to `SD:/system-boot/user-data`.

5. Insert the microSD card and turn on the Raspberry Pi. Wait a few minutes.

#### Configure with Ansible Playbook
6. Run the `lxd-cluster.yml` playbook to configure an LXD cluster between the two nodes.

        cd ansible/
        ansible-playbook playbooks/lxd-cluster.yml
