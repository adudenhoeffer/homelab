config:
  user.network-config: |
    version: 2
    ethernets:
      eth0:
        dhcp4: false
        addresses: [192.168.50.33/24]
        gateway4: 192.168.50.1
        nameservers:
          addresses: [192.168.50.1]
  user.user-data: |
    #cloud-config
    hostname: drone-agent
    manage_etc_hosts: true
    locale: "en_US.UTF-8"
    timezone: "America/New_York"
    users:
    - name: alex
      groups: sudo
      shell: /bin/bash
      sudo: ['ALL=(ALL) NOPASSWD:ALL']
      ssh-authorized-keys:
        - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDlR/xRVjROgs0H/aS1UOhf8RwpDHjcfa7ECt9QwZdCN6TqMZ25RrZmr+DnSyhShL7exd8fuABy6YaYoUJgyMiSiBGgi+UDzJbKUVgBbs+lHNQAx4l3pyC3z/NTTXzRczyZmy8GmNYuF9zL1xr6bVbeA95dfmE3jFu9K9mgYH1K2Xb7A/yAh+RDvYNJ4NFuEShiz5EMPVOdRGkcOjEo71JbB/FSRELA4HZwILZnfQcxoOP/GoYDoTYtvzYkyH+uWAnt32jgtBuzYS2ww+wc11draaYQQhtlHEfJl34qrjcyzffxpJiVSxmFGEzuMcQWF5rFlHnN25lu9i446yG/4viNooOHtstpeVbgH7jxQ5rucKv/Ihaho47G61ZR2R9foZtMYZOGOMebsf5bt/5Uzxnvrvgbi4DqcaW/2Dze0vcCQh0w39YIopGRy6Pc//FU4ByLs/0ul32fiB8DwOqNp8Dsyi2z23b25LNMWs7m1VZdW5HL8F80Ve2co0UNIJTcnr8= alex@thinkpad
    - name: ansible
      groups: sudo
      shell: /bin/bash
      sudo: ['ALL=(ALL) NOPASSWD:ALL']
      ssh-authorized-keys:
        - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDXvLLCqJ1tAK7xHOuQcix8ChClOGBxxWSqUb2a2yC2j91AQ83fd/W/cGtB1MjSF4YMFBk+Dlf9lp5GC1FgGIYBjAX3YJp38g/9/tIsnmVnhEISr/ib9M7TwUvJmA6KuHn3E7ASwUAK8hDFHYGlloMYZOit7+O5hedF0jJuHe5aAj1+/4POIitZmjPgm3LTCHwFgT9WHXwlnJq3qOzcrGXwM9qHK1Jl4xno+CvNGc0biTIBPGivTdiMpA/fky04phAqZBDIK5zIiooieT+9zyUgz6lLYxUlBpSbvzF0MoB9l9PqVJco1R+4PDTzYc9Lf8keSG6IxK1G4at3HRSGJ6B0HGLfRGgjQTXdSMRzPAh9nu8hGg78Zs54iLe5Gz/4eEbJcNRcu+c21OVGsJ+ohU1yws/7mZpx7uezsKwSlHLF/usgb4m1cIhe+9Nc66JzuxFQSsSHHIa/H13jPq8It2C97hdTheUtiLPAnRDNhiPk2A07Tei6NOcR0325xmP7Q6M= ansible@thinkpad
    package_update: true
    package_upgrade: true
    package_reboot_if_required: true
    packages:
      - openssh-server
    # Secure SSH on start up.
    runcmd:
      - sed -i -e '/^PasswordAuthentication/s/^.*$/PasswordAuthentication no/' /etc/ssh/sshd_config
      - sed -i -e '/^PermitRootLogin/s/^.*$/PermitRootLogin no/' /etc/ssh/sshd_config
      - restart ssh
description: lxd profile for drone-agent
devices:
  appdata:
    path: /opt/appdata
    source: /opt/appdata
    type: disk


